EAPI=5

inherit autotools

MY_P="${PV//_/-}"

DESCRIPTION="libsass command line driver"
HOMEPAGE="http://libsass.org"
SRC_URI="https://github.com/sass/${PN}/archive/${MY_P}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=" || ( =dev-libs/libsass-${PV} ) "
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}-${MY_P}"

src_prepare() {
	eautoreconf
}
