# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit savedconfig toolchain-funcs

DESCRIPTION="Dynamic virtual terminal manager"
HOMEPAGE="http://www.brain-dump.org/projects/dvtm/"

SRC_URI="http://www.brain-dump.org/projects/${PN}/${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~x86"

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND=">=sys-libs/ncurses-6.0:0=[unicode]"
RDEPEND=${DEPEND}

src_prepare() {
	sed -i \
		-e 's|FLAGS =|FLAGS +=|' \
		config.mk || die "sed config.mk failed"
	sed -i \
		-e '/strip/d' \
		-e '/STRIP/d' \
		Makefile || die "sed Makefile failed"

	restore_config config.h
}

src_compile() {
	local msg=""
	use savedconfig && msg=", please check the configfile"
	emake CC=$(tc-getCC) ${PN} || die "emake failed${msg}"
}

src_install() {
	emake DESTDIR="${D}" PREFIX="/usr" install

	insinto /usr/share/${PN}
	newins config.h ${PF}.config.h

	dodoc README.md

	save_config config.h
}

pkg_postinst() {
	elog "This ebuild has support for user defined configs"
	elog "Please read this ebuild for more details and re-emerge as needed"
	elog "if you want to add or remove functionality for ${PN}"
}
